//
//  ViewController.swift
//  Current Locatino
//
//  Created by Данил Черников on 1/14/19.
//  Copyright © 2019 Danil Chernikov. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController, CLLocationManagerDelegate{
    var mapView : GMSMapView?
    var locationManager = CLLocationManager()
    var didFindMyLocation = false
    
    override func loadView() {
        super.loadView()
        mapView = GMSMapView.map(withFrame: .zero, camera: .init())
        
        self.view = mapView!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        self.mapView!.addObserver(self, forKeyPath: "myLocation", options: NSKeyValueObservingOptions.new, context: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == CLAuthorizationStatus.authorizedWhenInUse{
            mapView!.isMyLocationEnabled = true
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if !didFindMyLocation{
            
            let myLocation = change![NSKeyValueChangeKey.newKey] as! CLLocation
            
            mapView!.camera = GMSCameraPosition.camera(withTarget: myLocation.coordinate, zoom: 10.0)
            mapView!.settings.myLocationButton = true
            
            didFindMyLocation = true
        }
    }
    
}
